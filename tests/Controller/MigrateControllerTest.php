<?php

namespace Drupal\migrate_sanity\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Provides automated tests for the migrate_sanity module.
 */
class MigrateControllerTest extends WebTestBase {

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "migrate_sanity MigrateController's controller functionality",
      'description' => 'Test Unit for module migrate_sanity and controller MigrateController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests migrate_sanity functionality.
   */
  public function testMigrateController() {
    // Check that the basic functions of module migrate_sanity.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
