<?php

namespace Drupal\migrate_sanity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\migrate_sanity\SanityFormatterInterface;

/**
 * Class MigrateController.
 */
class MigrateController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;


  /**
   * Drupal\migrate_sanity\SanityFormatterInterface definition.
   *
   * @var \Drupal\migrate_sanity\SanityFormatterInterface
   */
  protected $sanityFormatter;

  /**
   * Constructor function.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   Entity Manager.
   * @param \Drupal\migrate_sanity\SanityFormatterInterface $sanity_formatter
   *   Service class for format schema js files from content types.
   */
  public function __construct(EntityManagerInterface $entity_manager, SanityFormatterInterface $sanity_formatter) {
    $this->entityManager = $entity_manager;
    $this->sanityFormatter = $sanity_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('migrate_sanity.formatter')
    );
  }

  /**
   * Index.
   *
   * @return array
   *   Return a render array.
   */
  public function index() {
    $full_schema = $this->sanityFormatter->getFullContentSchema();
    $output = '';
    foreach ($full_schema as $schema) {
      $output .= <<<EOT
<div>
  <pre>
    <code>
{$schema}
    </code>
  </pre>
</div>
EOT;
    }
    return [
      '#type' => 'markup',
      '#markup' => $output,
    ];
  }

}
