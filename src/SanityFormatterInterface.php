<?php

namespace Drupal\migrate_sanity;

use Drupal\node\NodeTypeInterface;

/**
 * Interface SanityFormatterServiceInterface.
 */
interface SanityFormatterInterface {

  /**
   * Get schema.js content for all content types.
   *
   * @return string[]
   *   Return an array of strings. Each stirng is a schema.js file.
   */
  public function getFullContentSchema();

  /**
   * Get schema.js for a content type.
   *
   * @param \Drupal\node\NodeTypeInterface $entity_type
   *   Content type to generate schema from.
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $fields
   *   Array of FieldDefinition. All fields returned if NULL.
   *
   * @return string
   *   Formatted code string.
   */
  public function getContentSchema(NodeTypeInterface $entity_type, array $fields = NULL);

}
