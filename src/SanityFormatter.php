<?php

declare(strict_types = 1);

namespace Drupal\migrate_sanity;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\node\NodeTypeInterface;

/**
 * Class SanityFormatterService.
 */
class SanityFormatter implements SanityFormatterInterface {

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructor for for Sanity Formatter Class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   Entity manager interface.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity Field manager interface.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityManager = $entity_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFullContentSchema() : array {
    $output = [];
    $entity_types = $this->entityManager->getStorage('node_type')->loadMultiple();
    foreach ($entity_types as $entity_type) {
      $output[] = $this->getContentSchema($entity_type);
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentSchema(NodeTypeInterface $entity_type, array $fields = NULL) : string {
    // Load all fields if NULL.
    if ($fields === NULL) {
      $fields = $this->entityFieldManager->getFieldDefinitions('node', $entity_type->get('type'));
    }

    $output = <<<EOT
export default {
  name: '{$entity_type->get('type')}',
  title: '{$entity_type->get('name')}',
  type: 'document',
  fields: [

EOT;
    foreach ($fields as $field) {
      $output .= $this->getFormattedField($field);
    }

    if (!empty($fields)) {
      $output = substr($output, 0, strrpos($output, ','));
    }

    $output .= <<<EOT

  ]
}
EOT;
    return $output;
  }

  /**
   * Format Content Type fields.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field
   *   Content type field data.
   *
   * @return string
   *   formatted field.
   */
  protected function getFormattedField(FieldDefinitionInterface $field) : string {
    $field_name = $field->getName();
    $field_label = (string) $field->getLabel();
    $field_type = (string) $field->getType();
    switch ($field_name) {
      case 'title':
        return $this->getFormattedBasicField($field_name, $field_label, 'string');

      case 'created':
        return $this->getFormattedBasicField($field_name, $field_label, 'datetime');

      default:
        return $this->getFormattedBasicField($field_name, $field_label, $field_type);
    }
  }

  /**
   * Formatted string of content field.
   *
   * @param string $name
   *   String name field.
   * @param string $title
   *   String title field.
   * @param string $type
   *   String type field.
   *
   * @return string
   *   Formatted field string.
   */
  protected function getFormattedBasicField(string $name, string $title, string $type) : string {
    return <<<EOT
    {
      name: '{$name}',
      title: '{$title}',
      type: '{$type}'
    },

EOT;
  }

}
