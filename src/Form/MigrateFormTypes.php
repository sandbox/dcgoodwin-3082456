<?php

namespace Drupal\migrate_sanity\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class MigrateForm.
 */
class MigrateFormTypes extends MigrateFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_form_types';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#markup' => '<p>' . $this->t('Select the field types to generate migration files for.') . '</p>',
    ];

    $entity_types = $this->entityManager->getStorage('node_type')->loadMultiple();

    foreach ($entity_types as $name => $type) {
      $form['entity_types']['entity_type_' . $name] = [
        '#type' => 'checkbox',
        '#title' => $type->label(),
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Select'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($this->getSelectedTypes($form_state))) {
      $form_state->setError($form['entity_types'], $this->t('At least one content type must be selected.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->tempStore->get('migrate_sanity')->set('selected_types', $this->getSelectedTypes($form_state));
    $form_state->setRedirect('migrate_sanity.migrate_form_fields');
  }

  /**
   * Check to see which content types have been selected.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return string[]
   *   Array of select content types.
   */
  private function getSelectedTypes(FormStateInterface $form_state) {
    $selected_types = [];
    foreach ($form_state->getValues() as $key => $value) {
      if (strpos($key, 'entity_type_') === 0) {
        if ($value === 1) {
          $selected_types[] = str_replace('entity_type_', '', $key);
        }
      }
    }
    return $selected_types;
  }

}
