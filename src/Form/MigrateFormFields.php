<?php

namespace Drupal\migrate_sanity\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class MigrateForm.
 */
class MigrateFormFields extends MigrateFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_form_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['rename_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Rename Fields'),
      '#description' => $this->t('Change field names to camel case and remove leading &#039;Field_&#039;.'),
      '#weight' => '0',
    ];

    $selected_types = $this->tempStore->get('migrate_sanity')->get('selected_types');
    $entity_types = $this->entityManager->getStorage('node_type')->loadMultiple($selected_types);

    foreach ($entity_types as $name => $type) {
      $form[$name . 'description'] = [
        '#markup' => '<p><strong>' . $type->label() . '</strong></p>',
      ];
      $fields = $this->fieldManager->getFieldDefinitions('node', $name);
      foreach ($fields as $field_name => $field_value) {
        $form[$name . 'description']['field_name_' . $field_name] = [
          '#type' => 'checkbox',
          '#title' => $field_value->getLabel(),
        ];
      }
    }

    // $form['back'] = [
    //   '#type' => 'submit',
    //   '#value' => $this->t('Back'),
    // ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    dump($form_state->getValue($form['back']));
  }

}
